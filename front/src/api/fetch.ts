import axios, { AxiosResponse } from 'axios';
import { getCookie } from 'utils/cookie';

export const backendUrl = process.env.BACKEND || '';
const authToken = () => getCookie('auth') || null;

const headers = {
  Authorization: authToken(),
};

export const put = async <B, R>(url: string, data: B): Promise<AxiosResponse<B, R>> => {
  return axios({
    method: 'PUT',
    url: backendUrl + url,
    data,
    headers,
  });
};

export const post = async <B, R>(url: string, data: B): Promise<AxiosResponse<B, R>> => {
  return axios({
    method: 'POST',
    url: backendUrl + url,
    data,
    headers,
  });
};

export const get = async <B>(url: string): Promise<AxiosResponse<B>> => {
  return axios({
    method: 'GET',
    url: backendUrl + url,
    headers,
  });
};
