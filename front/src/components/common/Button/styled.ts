import styled from 'styled-components';
import { space } from 'utils/space';
import { ButtonProps } from './types';

export const ButtonStyled = styled.button<Omit<ButtonProps, 'children'>>(space, ({ variant }) => ({
  padding: '8px 12px',
  background: variant === 'primary' ? '#10629d' : '#555',
  border: '1px solid',
  borderColor: variant === 'primary' ? '#10629d' : '#555',

  borderRadius: 3,
  color: '#fff',

  ':hover': {
    background: variant === 'primary' ? '#1176bb' : '#666',
    cursor: 'pointer',
  },
}));
