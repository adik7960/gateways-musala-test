import { SpaceProps } from 'utils/space';

export interface ButtonProps extends SpaceProps {
  variant: 'primary' | 'secondary';
  children?: React.ReactNode;
}
