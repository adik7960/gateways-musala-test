import { space, SpaceProps } from 'utils/space';
import styled from 'styled-components';

export const Container = styled.div<SpaceProps>(space, {
  display: 'inline-block',
  width: '20px',
  height: '20px',
  cursor: 'pointer',
  verticalAlign: 'top',
  borderRadius: '6px',
});
