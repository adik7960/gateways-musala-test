import { SpaceProps } from 'utils/space';
import { Container } from './styled';

interface IProps extends SpaceProps {
  checked?: boolean;
  onChange?: (prevState?: boolean) => void;
}

const NoActive = ({ color = '#ffffff' }) => (
  <svg width="20" height="20" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
    <rect x="0.5" y="0.5" width="17" height="17" fill={color} stroke="#DDDDDD" />
  </svg>
);

const Active = ({ color = '#256FFF' }) => (
  <svg width="20" height="20" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
    <rect x="0" y="0" width="18" height="18" fill={color} stroke={color} />
    <path d="M4.15381 8.65396L7.75894 12.4617L14.1923 5.53857" stroke="white" strokeWidth="2" />
  </svg>
);

export const CheckBox: React.FC<IProps> = ({ checked, onChange }) => (
  <Container onClick={() => onChange?.(checked)}>{checked ? <Active /> : <NoActive />}</Container>
);
