import { ReactNode, useState } from 'react';
import { AnimatedContainer, Button, Container } from './styled';
import { ReactComponent as Chevron } from './chevron.svg';

interface Props {
  children?: ReactNode;
  title: ReactNode;
}

export const Collapse: React.FC<Props> = ({ children, title }) => {
  const [visible, setVisible] = useState(false);

  return (
    <>
      <Container>
        <Button onClick={() => setVisible(!visible)}>
          <IconWrapper rotate={visible}>
            <Chevron />
          </IconWrapper>
          {title}
        </Button>
      </Container>
      <AnimatedContainer show={visible}>{children}</AnimatedContainer>
    </>
  );
};
