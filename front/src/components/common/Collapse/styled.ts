import styled from 'styled-components';

export const Container = styled.div({
  fontSize: '16px',
});

export const Button = styled.button({
  border: 'none',
  padding: '8px 16px',
});

export const AnimatedContainer = styled.div<{ show: boolean }>(({ show }) => ({
  marginRight: '12px',
  opacity: show ? 1 : 0,
  marginTop: show ? 3 : 0,
  transition: show
    ? 'max-height 1s ease-in-out, margin 150ms linear, opacity 150ms linear'
    : 'max-height 500ms cubic-bezier(0, 1, 0, 1), margin 300ms ease-in-out, opacity 300ms ease-in-out',
  minHeight: 0,
  maxHeight: show ? 10000 : 0,
  overflow: 'hidden',
}));
