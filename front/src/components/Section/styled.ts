import styled from 'styled-components';

export const Container = styled.section({
  height: '80vh',
  minWidth: '300px',
  maxWidth: '700px',
  width: '100%',
  background: '#1e1e1e',
  borderRadius: '17px',
  border: '1px solid #414141',
  padding: '10px',
});
