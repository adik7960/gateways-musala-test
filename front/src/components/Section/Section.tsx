import { Collapse } from 'components/common/Collapse';
import { useState } from 'react';
import { ButtonsBar } from './ButtonsBar';
import { Content } from './Content';
import { Container } from './styled';

const data = [
  {
    id: 0,
    title: 'Gateway #1',
    devices: [
      {
        id: 0,
        title: 'Device #1',
      },
    ],
  },
  {
    id: 1,
    title: 'Gateway #2',
    devices: [
      {
        id: 0,
        title: 'Device #7960',
      },
    ],
  },
];

export const Section: React.FC = () => {
  const [data, setData] = useState(null);
  return (
    <Container>
      <ButtonsBar />
      <Content data={data} />
    </Container>
  );
};
