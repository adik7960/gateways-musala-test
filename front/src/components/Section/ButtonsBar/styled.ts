import { Button } from 'components/common/Button';
import styled from 'styled-components';

export const Container = styled.div({
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  overflowX: 'auto',
  padding: '10px',

  [`${Button}`]: {
    whiteSpace: 'nowrap',
  },
});
