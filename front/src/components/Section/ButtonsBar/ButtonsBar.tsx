import { Button } from 'components/common/Button';
import { CheckBox } from 'components/common/CheckBox';
import { Container } from './styled';

export const ButtonsBar: React.FC = () => {
  return (
    <Container>
      <CheckBox />
      <div>
        <Button ml={2} variant="primary">
          Add node
        </Button>
        <Button ml={2} variant="secondary">
          Dellete node
        </Button>
        <Button ml={2} variant="secondary">
          Rename node
        </Button>
      </div>
    </Container>
  );
};
