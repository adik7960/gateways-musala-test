import styled, { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle({
  body: {
    '-webkit-font-smoothing': 'antialiased',
    background: '#323233',
    color: '#fff',
    padding: '0 10px',
    margin: 0,
    fontFamily: '"Roboto", Tahoma',
    fontWeight: 400,
    fontSize: '14px',
    lineHeight: '18px',
    letterSpacing: '0.637px',
  },

  '*': {
    outlineColor: '#0772bc',
    boxSizing: 'border-box',
  },

  a: {
    textDecoration: 'none',
  },

  'input, button, select': {
    fontFamily: '"Roboto", Tahoma',
    fontWeight: 400,
    fontSize: '14px',
    lineHeight: '20px',
    letterSpacing: '0.75px',
  },
});

export const Container = styled.div({
  flexDirection: 'column',
  width: '100%',
  height: '100%',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
});
