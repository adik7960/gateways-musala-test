import { Header } from 'components/Header';
import { Section } from 'components/Section';
import { Container, GlobalStyle } from './styled';

export const App: React.FC = () => {
  return (
    <>
      <GlobalStyle />
      <Container>
        <Header />
        <Section />
      </Container>
    </>
  );
};
