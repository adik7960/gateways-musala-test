import { H1 } from './styled';

export const Header: React.FC = () => (
  <header>
    <H1>Gateways</H1>
  </header>
);
