import styled from 'styled-components';

export const H1 = styled.h1({
  lineHeight: '10vh',
  margin: 0,
});
