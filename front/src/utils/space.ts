export interface SpaceProps {
  mt?: number;
  mb?: number;
  mr?: number;
  ml?: number;
}

const MULTIPLIER = 4;

export const space = ({ mt, mb, mr, ml }: SpaceProps) => ({
  marginLeft: ml && ml * MULTIPLIER,
  marginRight: mr && mr * MULTIPLIER,
  marginTop: mt && mt * MULTIPLIER,
  marginBottom: mb && mb * MULTIPLIER,
});
