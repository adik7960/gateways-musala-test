const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ESLintPlugin = require('eslint-webpack-plugin');
// const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');

const exclude = /node_modules/;

const outputPath = path.resolve(__dirname, 'build');

module.exports = (env, args, name) => {
  const { mode } = args;
  const isProduction = mode === 'production';
  const isDevelopment = mode === 'development';

  return {
    mode,

    resolve: {
      extensions: ['.js', '.jsx', '.tsx', '.ts'],
      modules: ['src', 'node_modules'],
    },

    plugins: [
      new HtmlWebpackPlugin({
        template: './src/index.html',
      }),
      new ESLintPlugin({ extensions: ['ts', 'tsx', 'js', 'jsx'] }),
      // new BundleAnalyzerPlugin(),
    ],

    devtool: isDevelopment ? 'source-map' : false,
    devServer: {
      historyApiFallback: true,
      static: {
        directory: 'public',
        publicPath: '/',
      },
      port: '7900',
      hot: true,
      liveReload: true,
      client: {
        progress: true,
        overlay: false,
      },
      proxy: {
        '/back': {
          target: 'http://localhost:7960',
          changeOrigin: true,
          pathRewrite: { '^/back': '' },
        },
      },
    },

    entry: {
      main: path.resolve(__dirname, 'src/index.tsx'),
    },

    output: {
      publicPath: '/',
      path: outputPath,
      filename: isProduction ? '[name].[contenthash].js' : '[name].bundle.js',
      clean: true,
    },

    module: {
      rules: [
        {
          test: /\.(js|jsx|ts|tsx)$/,
          exclude,
          use: {
            loader: 'babel-loader',
            options: {
              presets: [
                ['@babel/preset-env'],
                [
                  '@babel/preset-react',
                  {
                    runtime: 'automatic',
                  },
                ],
                '@babel/preset-typescript',
              ],
            },
          },
        },
        {
          test: /\.svg$/i,
          type: 'asset',
          resourceQuery: /url/, // *.svg?url
        },
        {
          test: /\.svg$/,
          issuer: /\.[jt]sx?$/,
          resourceQuery: { not: [/url/] }, // exclude react component if *.svg?url
          use: [
            {
              loader: '@svgr/webpack',
              options: {
                svgoConfig: {
                  plugins: [
                    {
                      name: 'removeViewBox',
                    },
                  ],
                },
                ref: true,
              },
            },
            'file-loader',
          ],
        },
        {
          test: /\.(jpg|png|gif|ttf)$/,
          type: 'asset/resource',
        },
      ],
    },
  };
};
